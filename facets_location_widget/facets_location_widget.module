<?php
use Drupal\facets_location_widget\Form\TestForm;

/**
 * @file
 * Module file for the Facets Location Widget module.
 */

/**
 * Implements hook_help().
 */
function facets_location_widget_help($route_name, $route_match) {
  switch ($route_name) {
    case 'help.page.facets_location_widget':
      return '<p>' . t('Provides a location filter with address autocomplete and distance selection for Facets.') . '</p>';
  }
}

/**
 * Implements hook_theme().
 */
function facets_location_widget_theme() {
  return [
    'facets_location_widget' => [
      'render element' => 'widget',
    ],
    'facets__form' => [
      'render element' => 'form',
    ]
  ];
}

function buildWidgetForm() {
  $form['address'] = [
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#autocomplete_route_name' => 'facets_location_widget.autocomplete',
    '#required' => TRUE,
  ];

  $form['distance'] = [
    '#type' => 'select',
    '#title' => t('Distance'),
    '#options' => [
      '0' => t('Any distance'),
      '1' => t('1 km'),
      '3' => t('3 km'),
      '5' => t('5 km'),
      '10' => t('10 km'),
    ],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#ajax' => [
      'callback' => [TestForm::class, 'submitForm'],
      'wrapper' => 'results',
    ],
  ];
    
  return $form;
}