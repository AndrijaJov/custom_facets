/**
 * @file
 * Provides the slider functionality.
 */

(function ($) {

  'use strict';

  Drupal.facets = Drupal.facets || {};

  Drupal.behaviors.rangeInput = {
    attach: function (context, settings) {
      if (settings.facets !== 'undefined' && settings.facets.locationInput !== 'undefined') {
        $.each(settings.facets.locationInput, function (facet, settings) {
          Drupal.facets.locationInput(facet, settings);
        });
      }
    }
  };

  Drupal.facets.locationInput = function (facet, settings) {
    var fieldInputs = [$('#' + facet + '_address'),$('#' + facet + '_distance')];

    $.each(fieldInputs, function (index, value) {
      // Validate fields as they type or change.
      value.on('load keyup keypress change', function () {
        var val = $(this).val().trim().replace(/\$/g, '');
        var facetId = $(this).attr('facet-location-input-facet-id');
        var inputId = $(this).attr('facet-location-input-input-id');

        var validationId = $('#' + facetId + '_' + inputId + '_validation');

        // if(val.length > 0 && $.isNumeric(val) && val >= 0) {
        //   $(this).removeClass('facet-form-required');
        //   $(validationId).empty();
        // } else {
        //   $(this).addClass('facet-form-required');
        //   $(validationId).html('<div class="facets-range-input-valid-input">' + Drupal.t('Please enter a positive number.') + '</div>');
        // }

        var facetSettings = drupalSettings.facets.locationInput;
        var $widget = $('.js-facets-widget.js-facets-location_input');
        var href = facetSettings[validatedData.facetId].url.replace('__location_input_address__', validatedData.address).replace('__location_input_distance__', validatedData.distance);
        // Trigger the facet update.
        $widget.trigger('facets_filter', [ href ]);
      });

    });

    // Set the current values via js from facet settings if there are any.
    if(typeof settings.currentValues !== "undefined") {
      $.each(fieldInputs, function (index, value) {
        var inputId = $(this).attr('facet-location-input-input-id');
        // if(settings.currentValues[inputId].length > 0 && $.isNumeric(settings.currentValues[inputId])) {
          value.val(settings.currentValues[inputId]);
        // }
      });
    }
  };

})(jQuery);

(function ($) {
  // Add an arbitrary jQuery method to call on Drupal Ajax Form.
  $.fn.facetsLocationInputFilter = function (validatedData) {
    var facetSettings = drupalSettings.facets.locationInput;
    var $widget = $('.js-facets-widget.js-facets-location_input');
    var href = facetSettings[validatedData.facetId].url.replace('__location_input_address__', validatedData.address).replace('__location_input_distance__', validatedData.distance);
    // Trigger the facet update.
    $widget.trigger('facets_filter', [ href ]);
  };

})(jQuery);
