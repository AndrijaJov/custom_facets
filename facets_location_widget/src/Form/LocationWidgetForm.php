<?php

namespace Drupal\facets_location_widget\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;

class LocationWidgetForm extends FormBase {

  /**
   * The facet.
   *
   * @var \Drupal\facets\FacetInterface
   */
  protected FacetInterface $facet;

  /**
   * The widget configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * Creates a new RangeInputForm instance.
   *
   * @param \Drupal\facets\FacetInterface $facet
   *   The facet.
   * @param array $configuration
   *   The facet widget configuration.
   */
  public function __construct(FacetInterface $facet, array $configuration = []) {
    $this->facet = $facet;
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facets_location_input_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['facets_location_widget.form'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'] = [
      'js-facets-widget',
      'js-facets-' . $this->facet->getWidget()['type'],
    ];
    $form['#theme'] = 'facets_locatioin_input_form';

    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#autocomplete_route_name' => 'facets_location_widget.autocomplete',
      '#required' => TRUE,
    ];

    $form['distance'] = [
      '#type' => 'select',
      '#title' => $this->t('Distance'),
      '#options' => [
        '0' => $this->t('Any distance'),
        '1' => $this->t('1 km'),
        '3' => $this->t('3 km'),
        '5' => $this->t('5 km'),
        '10' => $this->t('10 km'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#ajax' => [
        'callback' => [$this, 'submitForm'],
        'wrapper' => 'results',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $address = $form_state->getValue('address');
    $range = $form_state->getValue('distance');
    $is_zipcode = false;

    if (ctype_digit($address)) {
      $is_zipcode = true;
    }

    // If address is zipcode, there should be more custom logic to convert it into a valid address

    $apiKey = $this->config('facets_location_widget.settings')->get('google_geocoding_api_key');
    $endpoint = 'https://maps.googleapis.com/maps/api/geocode/json';

    $params = [
      'address' => $address,
      'key' => $apiKey,
    ];

    $url = $endpoint . '?' . http_build_query($params);
    $response = \Drupal::httpClient()->get($url);
    $data = json_decode($response->getBody(), TRUE);

    $location = $data['results'][0]['geometry']['location'];
    $formatted_location = 'POINT (' . $location['lat'] . ' ' . $location['lng'] . ')';

    dpm($formatted_location);
    dpm('Range: ' . $range);

    parent::submitForm($form, $form_state);
  }

  /**
   * Ajax submit form logic.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    if ($this->validateForm($form, $form_state)) {
      $payload = [
        'address' => $form_state->getValue('address'),
        'distance' => $form_state->getValue('distance'),
        'facetId' => $this->facet->id(),
      ];
      $response->addCommand(new InvokeCommand(NULL, 'facetsLocationInputFilter', [$payload]));
      return $response;
    }
    else {
      $invalidRangeText = $this->t('Please enter a valid range');
      $response->addCommand(new HtmlCommand('#validationContainer', $invalidRangeText));
      return $response;
    }
  }

}
