<?php

namespace Drupal\facets_location_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facets_location_widget_test';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['facets_location_widget.test'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#autocomplete_route_name' => 'facets_location_widget.autocomplete',
      '#required' => TRUE,
    ];

    $form['distance'] = [
      '#type' => 'select',
      '#title' => $this->t('Distance'),
      '#options' => [
        '0' => $this->t('Any distance'),
        '1' => $this->t('1 km'),
        '3' => $this->t('3 km'),
        '5' => $this->t('5 km'),
        '10' => $this->t('10 km'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#ajax' => [
        'callback' => [$this, 'submitForm'],
        'wrapper' => 'results',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $address = $form_state->getValue('address');
    $range = $form_state->getValue('distance');
    $is_zipcode = false;

    if (ctype_digit($address)) {
      $is_zipcode = true;
    }

    // If address is zipcode, there should be more custom logic to convert it into a valid address

    $apiKey = $this->config('facets_location_widget.settings')->get('google_geocoding_api_key');
    $endpoint = 'https://maps.googleapis.com/maps/api/geocode/json';

    $params = [
      'address' => $address,
      'key' => $apiKey,
    ];

    $url = $endpoint . '?' . http_build_query($params);
    $response = \Drupal::httpClient()->get($url);
    $data = json_decode($response->getBody(), TRUE);

    $location = $data['results'][0]['geometry']['location'];
    $formatted_location = 'POINT (' . $location['lat'] . ' ' . $location['lng'] . ')';

    dpm($formatted_location);
    dpm('Range: ' . $range);

    parent::submitForm($form, $form_state);
  }

}
