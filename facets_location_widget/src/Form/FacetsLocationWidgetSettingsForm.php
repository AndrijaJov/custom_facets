<?php

namespace Drupal\facets_location_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for the Facets Location Widget module.
 */
class FacetsLocationWidgetSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facets_location_widget_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['facets_location_widget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('facets_location_widget.settings');

    $form['google_places_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Places API Key'),
      '#default_value' => $config->get('google_places_api_key'),
      '#required' => TRUE,
      '#description' => $this->t('Enter your Google Places API key.'),
    ];

    $form['google_geocoding_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Geocoding API Key'),
      '#default_value' => $config->get('google_geocoding_api_key'),
      '#required' => TRUE,
      '#description' => $this->t('Enter your Google Geocoding API key.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('facets_location_widget.settings')
      ->set('google_places_api_key', $form_state->getValue('google_places_api_key'))
      ->set('google_geocoding_api_key', $form_state->getValue('google_geocoding_api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
