<?php

namespace Drupal\facets_location_widget\Plugin\facets\widget;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Widget\WidgetPluginBase;
use Drupal\facets_location_widget\Form\LocationWidgetForm;
use Drupal\facets_location_widget\Form\TestForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A simple widget class that returns a simple array of the facet results.
 *
 * @FacetsWidget(
 *   id = "location_input",
 *   label = @Translation("Location Input"),
 *   description = @Translation("A widget that provides location input."),
 * )
 */
class FacetsLocationWidget extends WidgetPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * Creates a RangeInputWidget plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
      return [];
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet): array {
    $build = parent::build($facet);

    $results = $facet->getResults();
    ksort($results);

    // Make sure there are results before building up the facet settings,
    // otherwise just return the build since we can't do anything to it without
    // a placeholder URL... and there's no results to facet anyway.
    if (empty($results)) {
      return $build;
    }
    else {
      // Get only the first result.
      $firstResult = reset($results);
      // Build a generic URL with placeholders.
      $url = $firstResult->getUrl()->toString();

      $build['#attached']['library'][] = 'facets_location_widget/facetsLocationWidget';

      // Is there an active range set up already?
      $active = $facet->getActiveItems();

      $facet_settings = &$build['#attached']['drupalSettings']['facets']['facetsLocationWidget'][$facet->id()];
      $facet_settings['facetId'] = $facet->id();
      $facet_settings['url'] = $url;
      unset($facet_settings['value']);

      // $build['form'] = $this->formBuilder->getForm(new RangeInputForm($facet, $this->getConfiguration()));
      $build['form'] = $this->formBuilder->getForm(new LocationWidgetForm($facet, $this->getConfiguration()));

      $build['type'] = [
        '#markup' => str_replace('_', '-', $facet->getWidget()['type']),
      ];
      return $build;
    }

  }

}
