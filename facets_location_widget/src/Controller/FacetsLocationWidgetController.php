<?php

namespace Drupal\facets_location_widget\Controller;

use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Controller for handling autocomplete requests and filtering results.
 */
class FacetsLocationWidgetController extends ControllerBase {

  protected $httpClient;
  protected $googlePlacesApiKey;
  protected $googleGeocodingApiKey;

  public function __construct(ClientInterface $httpClient) {
    $this->httpClient = $httpClient;
    // $this->googlePlacesApiKey = $google_places_api_key;
    // $this->googleGeocodingApiKey = $google_geocoding_api_key;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      // $container->getParameter('google_places_api_key'),
      // $container->getParameter('google_geocoding_api_key')
    );
  }

  /**
   * Callback for autocomplete suggestions.
   */
  public function autocomplete(Request $request) {
    $response = new JsonResponse();
    $input = $request->query->get('q');

    if (!empty($input)) {
      $apiKey = $this->config('facets_location_widget.settings')->get('google_places_api_key');
      $endpoint = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
      $params = [
        'input' => $input,
        'key' => $apiKey,
      ];

      // Make a request to Google Places API.
      $url = $endpoint . '?' . http_build_query($params);
      $placesResponse = $this->httpClient->get($url);
      $placesData = json_decode($placesResponse->getBody(), TRUE);

      // Extract predictions from the response.
      $predictions = [];
      if (!empty($placesData['predictions'])) {
        foreach ($placesData['predictions'] as $prediction) {
          $predictions[] = $prediction['description'];
        }
      }

      $response->setData($predictions);
    }

    return $response;
  }

  public function test() {
    $apiKey = $this->config('facets_location_widget.settings')->get('google_places_api_key');
    $endpoint = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
    $params = [
      'input' => "vojvode tankosica",
      'key' => $apiKey,
    ];

    // Make a request to Google Places API.
    $url = $endpoint . '?' . http_build_query($params);
    $placesResponse = $this->httpClient->get($url);
    $placesData = json_decode($placesResponse->getBody(), TRUE);

    // Extract predictions from the response.
    $predictions = [];
    if (!empty($placesData['predictions'])) {
      foreach ($placesData['predictions'] as $prediction) {
        $predictions[] = $prediction['description'];
      }
    }

    return ['#markup' => print_r($predictions, true)];
  }

}
