<?php

namespace Drupal\facets_range_input\Plugin\facets\widget;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets_range_input\Form\RangeInputForm;
use Drupal\facets\FacetInterface;
use Drupal\facets\Widget\WidgetPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A simple widget class that returns a simple array of the facet results.
 *
 * @FacetsWidget(
 *   id = "range_input",
 *   label = @Translation("Range Input"),
 *   description = @Translation("A widget that creates two input forms for min and max to create a range."),
 * )
 */
class RangeInputWidget extends WidgetPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * Creates a RangeInputWidget plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'minimum_title' => 'Minimum',
      'maximum_title' => 'Maximum',
      'minimum_placeholder' => 'Min',
      'maximum_placeholder' => 'Max',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
    $config = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state, $facet);

    $form['minimum_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum - title'),
      '#description' => $this->t('The title to show for the range minimum input field.'),
      '#default_value' => $config['minimum_title'],
    ];

    $form['maximum_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum - title'),
      '#description' => $this->t('The title to show for the range maximum input field.'),
      '#default_value' => $config['maximum_title'],
    ];

    $form['minimum_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum - placeholder'),
      '#description' => $this->t('The placeholder to show for the range minimum input field.'),
      '#default_value' => $config['minimum_placeholder'],
    ];

    $form['maximum_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum - placeholder'),
      '#description' => $this->t('The placeholder to show for the range maximum input field.'),
      '#default_value' => $config['maximum_placeholder'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet): array {
    $build = parent::build($facet);

    $results = $facet->getResults();
    ksort($results);

    // Make sure there are results before building up the facet settings,
    // otherwise just return the build since we can't do anything to it without
    // a placeholder URL... and there's no results to facet anyway.
    if (empty($results)) {
      return $build;
    }
    else {
      // Get only the first result.
      $firstResult = reset($results);
      // Build a generic URL with placeholders.
      $url = $firstResult->getUrl()->toString();

      $build['#attached']['library'][] = 'facets_range_input/rangeInput';

      // Is there an active range set up already?
      $active = $facet->getActiveItems();

      $facet_settings = &$build['#attached']['drupalSettings']['facets']['rangeInput'][$facet->id()];
      // Set the min value or placeholder.
      if (isset($active[0][0])) {
        $facet_settings['currentValues']['minimum'] = $active[0][0];
      }
      // Set the max value or placeholder.
      if (isset($active[0][1])) {
        $facet_settings['currentValues']['maximum'] = $active[0][1];
      }

      $facet_settings['facetId'] = $facet->id();
      $facet_settings['url'] = $url;
      unset($facet_settings['value']);

      $build['#theme'] = 'facets_range_input';
      $build['form'] = $this->formBuilder->getForm(new RangeInputForm($facet, $this->getConfiguration()));

      $build['type'] = [
        '#markup' => str_replace('_', '-', $facet->getWidget()['type']),
      ];
      return $build;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType(): ?string {
    return 'range';
  }

}
